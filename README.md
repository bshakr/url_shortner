# URL Shortner

This app is written using Rails 5 for the api and React for the frontend.

## Running the backend

To run rails app from `./api`

```
bundle
bundle exec rails s
```

The api needs a header with content type json:
`curl localhost:3000 -H "Content-Type: application/json" -XPOST -d '{ "url": "http://www.farmdrop.com" }'`

## Running the tests

- Run `bundle exec rspec spec` from inside `./api`

## Running the frontend

To run the frontend from './frontend'

```
yarn
yarn start
```

The frontend is served on port `3001`, to avoid collision with rails.


# Known issues

- No url field validation
- No tests on frontend
- No end to end tests
