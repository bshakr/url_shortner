Rails.application.routes.draw do
  get '/:key', to: 'urls#show', as: 'key'
  post '/', to: 'urls#create'
end
