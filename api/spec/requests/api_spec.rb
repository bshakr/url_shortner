require 'rails_helper'

describe 'api', type: :request do
  describe 'GET /:key' do
    it 'redirects to full url' do
      url = 'http://farmdrop.com'
      post '/', params: { url: url }
      short_url = JSON.parse(response.body)['short_url']
      get "/#{short_url.last(8)}"

      expect(response).to redirect_to(url)
    end
  end

  describe 'shortening url' do
    it 'gives back shortened url' do
      post '/', params: { url: 'http://farmdrop.com' }

      parsed_body = JSON.parse(response.body)
      expected = request.url.length + 8
      expect(parsed_body['short_url'].length).to eq expected
    end
  end
end
