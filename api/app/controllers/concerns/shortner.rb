module Shortner
  extend ActiveSupport::Concern

  def shorten(url)
    key = generate_key
    Rails.cache.write(key, url)
    key
  end

  private

  def generate_key
    key = generate_token

    while Rails.cache.read(key).present?
      key = generate_token
    end

    key
  end

  def generate_token
    SecureRandom.urlsafe_base64(6)
  end
end
