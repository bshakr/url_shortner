class UrlsController < ApplicationController
  include Shortner

  def show
    url = Rails.cache.read(url_params[:key])
    redirect_to url, status: 301
  end

  def create
    key = shorten(url_params[:url])
    render json: { short_url: key_url(key) }
  end


  private

  def url_params
    params.permit(:key, :url)
  end
end
