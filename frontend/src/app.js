import React, { Component } from 'react'
import axios from 'axios'
import clipboard from 'clipboard-polyfill'

import styles from './app.css'

class App extends Component {
  state = {
    url: null,
    shortUrl: null,
    copied: false
  }

  submit = (e) => {
    e.preventDefault()

    axios
    .post('http://localhost:3000/', { url: this.state.url })
    .then((response)=> {
      this.setState({shortUrl: response.data.short_url})
    })
  }

  copyToClipboard = (e) => {
    clipboard.writeText(this.state.shortUrl)
    this.setState({ copied: true })
    setTimeout(()=> this.setState({copied: false}), 2000)
  }

  handleInputChange = (e) => {
    this.setState({'url': e.target.value})
  }

  render() {
    return (
      <div className={styles.container}>
        <h1 className={styles.title}>Shorten</h1>
        <form onSubmit={this.submit}>
          <input
            name='url'
            onChange={this.handleInputChange}
            placeholder='Enter a url to shorten'
            className={styles.urlInput}
          />
        </form>

        {this.state.shortUrl &&
          <div
            onClick={this.copyToClipboard}
            className={styles.shortUrl}>
            {this.state.shortUrl}
          </div>}

        {this.state.copied &&
          <div className={styles.notificationContainer}>
            <div className={styles.notification}>
              Url copied to clipboard.
            </div>
          </div>}
      </div>
    );
  }
}

export default App;
